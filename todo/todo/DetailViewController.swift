//
//  DetailViewController.swift
//  todo
//
//  Created by Vu, Dinh Van  on 12/2/20.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var enterView: EnterView!
    @IBOutlet weak var removeText: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        let view = UIView(frame: CGRect(x: 0, y: removeText.bounds.height/2, width: removeText.bounds.width, height: 1))
        view.backgroundColor = UIColor.gray
        removeText.addSubview(view)
    }
    
    @IBAction func back(sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
