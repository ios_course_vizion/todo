//
//  EnterView.swift
//  todo
//
//  Created by Vu, Dinh Van  on 12/2/20.
//

import UIKit

class EnterView: UIView {

    override func layoutSubviews() {
        super.layoutSubviews()
        self.roundCorners(corners: [.topLeft, .topRight], radius: 10)
    }

}
